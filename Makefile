#!/usr/bin/make -f

PACKAGE_NAME = lenovo-yoga-utils
DEBIAN_CHANGELOG = debian/changelog
DEBIAN_CHANGELOG_VERSION = $(shell perl -ne 'if(s/^$(PACKAGE_NAME)\s*\((.*?)\).*$$/$$1/g){print;exit}' $(DEBIAN_CHANGELOG))
PACKAGE_VERSION = $(DEBIAN_CHANGELOG_VERSION)
PACKAGE_DATE = $(shell date +%F)

MANDIR = man

# all markdown manpages
MDMANPAGES = $(shell find $(MANDIR) -type f -iname '*.1.md')
# corresponding groff manpages
GFMANPAGES = $(MDMANPAGES:.1.md=.1)

PANDOCOPTS = -f markdown -t man --standalone -Vfooter='Version $(PACKAGE_VERSION)' -Vdate='$(PACKAGE_DATE)'

all: manpages

.PHONY: manpages
manpages: $(GFMANPAGES)

# manpages:
%.1: %.1.md $(DEBIAN_CHANGELOG)
	pandoc $(PANDOCOPTS) -o $@ $<

.PHONY: clean
clean:
	rm -f $(GFMANPAGES)

.PHONY: debclean
debclean:
	rm -rf $(addprefix $(DEBIAN)/,files *.substvars *.debhelper $(PACKAGE_NAME) debhelper-build-stamp *.debhelper.log)

.PHONY: distclean
distclean: clean debclean

