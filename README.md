# Lenovo Yoga Utilities

Support for convertibles is limited in GNU/Linux systems.

This package attempts to provide utilities that make the Lenovo Yoga series
(specifically the Lenovo Yoga 300) more useful with Debian/Ubuntu.

## Features

- `.desktop` files for **normal**, **tablet** and **tent** **mode**.

## Installation

### Debian package

There are ready-to-use debian packages on the
[tags page](https://gitlab.com/nobodyinperson/lenovo-yoga-utils/tags).
Download the package from there and install it with

```bash
sudo dpkg -i lenovo-yoga-utils*.deb
```

### APT repository

If you use my [APT repository](https://apt.nobodyinperson.de), install
`lenovo-yoga-utils` like any other package with

```bash
sudo apt-get update
sudo apt-get install lenovo-yoga-utils
```

## Credits/Reference

This program partly bases on the following projects:

- https://github.com/admiralakber/thinkpad-yoga-scripts
- https://gist.github.com/anonymous/5d2c2d2967eac8774b69

## Disclaimer

This software is provided as-is without **any warranty**. See `LICENSE` for more information.

The author is not in any way affiliated with the manufacturer [Lenovo](lenovo.com).
