% lenovo-yoga-tablet-mode(1) | Lenovo Yoga - tablet mode

NAME
====

**lenovo-yoga-tablet-mode** - use your Lenovo Yoga in tablet mode


SYNOPSIS
========

usage: lenovo-yoga-tablet-mode


AUTHOR
======

Yann Büchau <nobodyinperson@gmx.de>


