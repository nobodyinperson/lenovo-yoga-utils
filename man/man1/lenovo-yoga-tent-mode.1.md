% lenovo-yoga-tent-mode(1) | Lenovo Yoga - tent mode

NAME
====

**lenovo-yoga-tent-mode** - use your Lenovo Yoga in tent mode


SYNOPSIS
========

usage: lenovo-yoga-tent-mode


AUTHOR
======

Yann Büchau <nobodyinperson@gmx.de>


