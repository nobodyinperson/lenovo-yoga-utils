% lenovo-yoga-normal-mode(1) | Lenovo Yoga - normal mode

NAME
====

**lenovo-yoga-normal-mode** - use your Lenovo Yoga in normal mode


SYNOPSIS
========

usage: lenovo-yoga-normal-mode


AUTHOR
======

Yann Büchau <nobodyinperson@gmx.de>


